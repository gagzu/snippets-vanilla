((d) => {
	d.addEventListener('DOMContentLoaded', () => {
		let moving = false;
		let oldScrollPosition = 0;
		const header = d.getElementById('main-navbar');

		function animateHeader(currentScrollPosition) {
			let isFixed = header.classList.contains('fixed');
			let isHideFixed = header.classList.contains('hide-fixed');

			if(currentScrollPosition > 0) {
				if(!isFixed) {
					header.classList.add('fixed');
				}
			} else {
				header.classList.remove('fixed');
			}

			if( currentScrollPosition < oldScrollPosition ) {

				if(isHideFixed) {
					header.classList.remove('hide-fixed');
				}
			} else if(!isHideFixed) {
				header.classList.add('hide-fixed');
			}

			oldScrollPosition = currentScrollPosition;
		}

		window.addEventListener('scroll', function(e) {
			currentScrollPosition = window.scrollY;

			if (!moving) {
				window.requestAnimationFrame(function() {
					animateHeader(currentScrollPosition);
					moving = false;
				});

				moving = true;
			}
		});
	})
})(document);